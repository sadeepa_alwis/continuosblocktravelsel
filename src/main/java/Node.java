public record Node (boolean corner,
                    boolean edge,
                    int x,
                    int y,
                    String color
//                    Node up,
//                    Node left
){}
