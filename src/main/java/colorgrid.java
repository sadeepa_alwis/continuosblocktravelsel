import javax.sound.midi.Soundbank;

public class colorgrid {
    public static void main(String[] args) {

        int GridXAxis = 10;
        int GridYAxis = 20;

        Grid a = new Grid(GridXAxis,GridYAxis);
        Node[][] ex = a.create();
        a.display(ex);
//        a.display(a.dummy());
//        new Search().search(a.dummy(), 3,4);

        String ContinousBlockColor = new Search().search(ex,GridXAxis,GridYAxis);

        System.out.println();
        System.out.println();
        System.out.println("Continuous Block Color: "+ ContinousBlockColor);
    }
}
