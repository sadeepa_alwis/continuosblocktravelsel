import java.util.Arrays;
import java.util.Random;

public record Grid(int x, int y) {

    //assumed only 3 random colors
    static String[] colors = {"Gery","Gold","Blue"};

    Node[][] create(){

        Node grid[][];
        if(x<1 || y <1){
            grid = new Node[2][2];
        }else {
            grid = new Node[x][y];
        }

        int Xaxis = x-1;
        int Yaxix = y-1;

        Random colorPicker = new Random();

        for(int i = 0; i<x; i++){
//            System.out.print(i+" -->");
            for(int j = 0; j<y; j++){
//                System.out.println(j);

//                Node upNode = null;
//                Node leftNode = null;
//                if(i!=0){
//                    upNode = grid[--i][j];
//                }
//                if(j!=0){
//                    leftNode = grid[i][--j];
//                }

                if ((i==Xaxis && j==0) || (i==0 && j==Yaxix) || (i==Xaxis && j==Yaxix) || (i==0 && j==0)){
                    grid[i][j] = new Node(true,false,i,j, colors[colorPicker.nextInt(colors.length)]);
                }else{
                    grid[i][j] = new Node(false,false,i,j, colors[colorPicker.nextInt(colors.length)]);
                }
            }
        }
//        display(grid);
//        display(dummy());
//        return dummy();
        return grid;
    }

    void display(Node[][] e){

        Arrays.stream(e).toList().forEach( f-> {
            System.out.println();
            Arrays.stream(f).toList().forEach(g -> System.out.print(g.color()+ "\t"));});
    }

    Node[][] dummy(){

        String[] colors = {"Red1","Gren","Blue"};
        Node[][] dummy = new Node[3][4];

        dummy[0][0] = new Node(true,false,0,0, colors[1]);
        dummy[0][1] = new Node(false,true,0,1, colors[1]);
        dummy[0][2] = new Node(true,true,0,2, colors[2]);
        dummy[0][3] = new Node(true,true,0,2, colors[0]);
        dummy[1][0] =new Node(false,true,1,0, colors[2]);
        dummy[1][1] =new Node(false,true,1,1, colors[1]);
        dummy[1][2] =new Node(false,true,1,2, colors[0]);
        dummy[1][3] =new Node(false,true,1,2, colors[2]);
        dummy[2][0] =new Node(true,true,2,0, colors[1]);
        dummy[2][1] =new Node(false,true,2,1, colors[1]);
        dummy[2][2] =new Node(true,true,2,2, colors[1]);
        dummy[2][3] =new Node(true,true,2,2, colors[0]);

        return dummy;
    }
}
