import java.util.*;

public class Search {

    String search(Node[][] grid, int x, int y) {

        Map<String, Result> results = new HashMap<>();

        for (int i = 0; i < x; i++) {

            for (int j = 0; j < y; j++) {

                String color = grid[i][j].color();

                Validator validate = traverse(grid, i, j, color);

//                System.out.println(validate);
                Result e = results.get(color);

                if (e == null) {
                    results.put(color, new Result(1, color));
                } else {
                    if (validate.down() || validate.right()){
                        results.put(color, new Result(e.count() + 1, color));
                    }
                }
            }
        }


//        System.out.println();
//        System.out.println(results.get("Gren"));
//        System.out.println(results.get("Blue"));
//        System.out.println(results.get("Red1"));
        return results.values().stream().max(Comparator.comparingInt(Result::count)).orElseThrow(() -> new RuntimeException("No Continuous Color blocks found")).color();
    }

    Validator traverse(Node[][] grid, int x, int y, String color) {
        boolean down = false;
        boolean right = false;
        try {
            if (Objects.equals(color, grid[x][y + 1].color())) {
                right = true;
            }
            if (Objects.equals(color, grid[x + 1][y].color())) {
                down = true;
            }
        }catch (ArrayIndexOutOfBoundsException e){
//            System.out.println("x = "+x + " y = "+ y);
        }
        return new Validator(down, right);

    }

}

record Validator(
        boolean down,
        boolean right) {
}